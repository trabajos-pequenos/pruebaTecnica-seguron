# Prueba Técnica Seguron

Prueba técnica que consiste en la creación de un api con laravel que consuma data y que este páginada, todo esto enviado a la vista en cualquier front end que se encuentre comodo, finalmente subirlo a GitLab.

## Intrucciones para llenar BD
Se uso Apache y MySQL de XAMPP
 
1. Crear una base de datos llamada **laravel9** en PHPMYADMIN (http://localhost/phpmyadmin/) o configurar otra BD en .env.

2. Ejecutar el Comando **php artisan migrate:fresh** para crear la tabla de students en BD.

3. Ejecutar el comando **php artisan db:seed** para llenar la base de datos con estudiantes.

