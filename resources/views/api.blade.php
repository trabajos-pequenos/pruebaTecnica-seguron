<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Api Estudiantes</title>
    <link rel="stylesheet" href="{{ asset('css/api.css') }}">
</head>
<body>
    <main>
        <h1>Estudiantes API</h1>

        <div class="pagination">
            <!-- Botones para controlar páginado -->
            @if ($data['students']->onFirstPage())
                <span class="pagination-arrow">&laquo;</span>
            @else
                <a class="pagination-arrow active" href="{{ $data['students']->previousPageUrl() }}" rel="prev">&laquo;</a>
            @endif

            @for ($i = 1; $i <= $data['students']->lastPage(); $i++)
                <a href="{{ $data['students']->url($i) }}" class="pagination-text {{ $data['students']->currentPage() == $i ? 'active' : '' }}">{{ $i }}</a>
            @endfor

            @if ($data['students']->hasMorePages())
                <a class="pagination-arrow active" href="{{ $data['students']->nextPageUrl() }}" rel="next">&raquo;</a>
            @else
                <span class="pagination-arrow">&raquo;</span>
            @endif

        </div>

            <div class="grid"> 
                <!-- foreach para mostrar los datos del API -->
                @foreach($data['students'] as $student)

                        <div class="box-student">
                            <h2>{{$student->Nombre}} {{$student->Apellido}}</h2>
                            <img src="{{ asset('img/default-user.webp') }}" alt="default user icon">
                            <p>Curso: {{$student->Curso}}</p>
                            <p>Correo: {{$student->Correo}}</p>
                        </div>
                    
                @endforeach
            </div>
    </main>
</body>
</html>