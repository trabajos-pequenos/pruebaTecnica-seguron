<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Usando librearia Faker para generar nombres y apellidos aleatorios.
        $faker = Faker::create();

        // Aca se van a generar 100 estudiantes, el número se puede cambiar para seleccionar la cantidad deseada.
        foreach (range(1,100) as $index) {
            $nombre = $faker->firstName;
            $apellido = $faker->lastName;
            $curso = $faker->randomElement(['Informática', 'Agricultura', 'Cocina', 'Medicina']);
            $correo = strtolower($nombre) . strtolower($apellido) . $faker->unique()->randomNumber(2) . '@gmail.com';

        // Se llena la tabla previamente configurada como laravel9
            DB::table('students')->insert([
                [
                    'Nombre' => $nombre,
                    'Apellido' => $apellido,
                    'Curso' => $curso,
                    'Correo' => $correo
                ]
            ]);
        }

    }
}
