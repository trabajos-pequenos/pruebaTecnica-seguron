<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    // creación de la tabla students
    protected $table = 'students';
    // creación de los atributos de students
    protected $fillable = [
        'Nombre',
        'Apellido',
        'Curso',
        'Correo'
    ];
}
