<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Student;

class StudentController extends Controller
{
    public function index() {
        // paginado configurado para que muestre 9 objetos por página
        $students = Student::paginate(9);

        if($students->count() > 0) {
            $data = [
                'status' => 200,
                'students' => $students 
            ];
            // Retorna/redirecciona a la view api.blade.php y entrega la información de la variable $students
            return view('api',['data' => $data]);
        } else {
            // En caso de que no se encuentres datos, debido a error del seeder o algun error que no permita encontrar el api.
            $data = [
                'status' => 404,
                'message' => 'No se encontraron datos'
            ];
            return response()->json($data, 404);
        }
    }
}
