<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// al acceder al url principal / se activa la clase index() del StudentController.
Route::get('/', [StudentController::class, 'index']);